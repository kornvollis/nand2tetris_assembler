package com.pv.nandtotetris.service;

import com.pv.nandtotetris.exceptions.InvalidInstructionException;
import com.pv.nandtotetris.fields.Comp;
import com.pv.nandtotetris.fields.Dest;
import com.pv.nandtotetris.fields.Jump;
import com.pv.nandtotetris.instructions.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;


/**
 * Created by potharnv on 2016.11.24..
 */
public class InstructionConverterImplTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private InstructionConverter instructionConverter = new InstructionConverterImpl();

    @Test
    public void invalidInstructionShouldThrowInvalidInstructionException() {
        exception.expect(InvalidInstructionException.class);
        instructionConverter.parseInstruction("    //  asdas asd asdas dasda");
    }

    @Test
    public void invalidInstructionShouldThrowInvalidInstructionException2() {
        exception.expect(InvalidInstructionException.class);
        instructionConverter.parseInstruction("kaka");
    }

    @Test
    public void parseAInstruction() {
        AInstruction hackInstruction = (AInstruction) instructionConverter.parseInstruction("@16");
        assertEquals(16, hackInstruction.getValue());
    }

    @Test // D=M
    public void cInstruction1() {
        CInstruction hackInstruction = (CInstruction) instructionConverter.parseInstruction("D=M");
        assertEquals(Dest.D, hackInstruction.getDest());
        assertEquals(Comp.M, hackInstruction.getComp());
        assertEquals(Jump.NULL, hackInstruction.getJmp());
    }

    @Test // D=M;JEQ
    public void cInstruction2() {
        CInstruction hackInstruction = (CInstruction) instructionConverter.parseInstruction("D=M;JEQ");
        assertEquals(Dest.D, hackInstruction.getDest());
        assertEquals(Comp.M, hackInstruction.getComp());
        assertEquals(Jump.JEQ, hackInstruction.getJmp());
    }

    @Test // A=M+1;JLT
    public void cInstruction3() {
        CInstruction hackInstruction = (CInstruction) instructionConverter.parseInstruction("A=M+1;JLT");
        assertEquals(Dest.A, hackInstruction.getDest());
        assertEquals(Comp.M_PLUS_1, hackInstruction.getComp());
        assertEquals(Jump.JLT, hackInstruction.getJmp());
    }

    @Test // AMD=D|A;JGE
    public void cInstruction4() {
        CInstruction hackInstruction = (CInstruction) instructionConverter.parseInstruction("AMD=D|A;JGE");
        assertEquals(Dest.AMD, hackInstruction.getDest());
        assertEquals(Comp.D_OR_A, hackInstruction.getComp());
        assertEquals(Jump.JGE, hackInstruction.getJmp());
    }

    @Test
    public void label() {
        LabelInstruction hackInstruction = (LabelInstruction) instructionConverter.parseInstruction("(LABEL)");
        assertEquals("LABEL", hackInstruction.getName());
    }
}