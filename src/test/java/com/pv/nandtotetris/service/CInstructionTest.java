package com.pv.nandtotetris.service;

import com.pv.nandtotetris.fields.Comp;
import com.pv.nandtotetris.fields.Dest;
import com.pv.nandtotetris.fields.Jump;
import com.pv.nandtotetris.instructions.CInstruction;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by potharnv on 2016.11.30..
 */
public class CInstructionTest {

    @Test // D=M
    public void D_eq_M() {
        CInstruction cInstruction = new CInstruction(Dest.D, Comp.M, Jump.NULL);
        assertEquals("1111110000010000", cInstruction.getCode());
    }

    @Test // D=D-M
    public void D_eq_D_min_M() {
        CInstruction cInstruction = new CInstruction(Dest.D, Comp.D_MINUS_M, Jump.NULL);
        assertEquals("1111010011010000", cInstruction.getCode());
    }

    @Test // D;JGT
    public void D_JGT() {
        CInstruction cInstruction = new CInstruction(Dest.NULL, Comp.D, Jump.JGT);
        assertEquals("1110001100000001", cInstruction.getCode());
    }

    @Test // 0;JMP
    public void ZERO_JMP() {
        CInstruction cInstruction = new CInstruction(Dest.NULL, Comp.ZERO, Jump.JMP);
        assertEquals("1110101010000111", cInstruction.getCode());
    }

    @Test // M=D
    public void M_eq_D() {
        CInstruction cInstruction = new CInstruction(Dest.M, Comp.D, Jump.NULL);
        assertEquals("1110001100001000", cInstruction.getCode());
    }
}