package com.pv.nandtotetris.service;

import com.pv.nandtotetris.instructions.AInstruction;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by potharnv on 2016.11.24..
 */
public class AInstructionTest {

    @Test
    public void getCode()  {
        assertEquals("0000000000000000", new AInstruction(0).getCode());
        assertEquals("0000000000001000", new AInstruction(8).getCode());
        assertEquals("0001011100000110", new AInstruction(5894).getCode());
    }

}