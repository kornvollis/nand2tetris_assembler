package com.pv.nandtotetris.service;

import org.junit.Test;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Created by potharnv on 2016.12.02..
 */
public class SourceCleanerTest {

    private SourceCleaner sourceCleaner = new SourceCleaner();

    @Test
    public void cleanSource() throws Exception {
        URL url = this.getClass().getClassLoader().getResource("Max.asm");
        String fileName = url.getFile();
        fileName = System.getProperty( "os.name" ).contains( "indow" ) ? fileName.substring(1) : fileName;

        Stream<String> stream = Files.lines(Paths.get(fileName));

        List<String> res = sourceCleaner.cleanSource(stream);
        int x = 1;
    }

}