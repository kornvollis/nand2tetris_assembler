package com.pv.nandtotetris.symbol;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by potharnv on 2016.12.01..
 */
public class SymbolTableTest {

    private SymbolTable symbolTable = new SymbolTable();

    @Test
    public void lookUpR0() {
        assertEquals(new Integer(0), symbolTable.lookUp("R0"));
    }

    @Test
    public void lookUpMissingValue() {
        assertEquals(null, symbolTable.lookUp("MissingKey"));
    }

    @Test
    public void putSymbol() {
        symbolTable.addSymbol("X", 73);
        assertEquals(new Integer(73), symbolTable.lookUp("X"));
    }
}