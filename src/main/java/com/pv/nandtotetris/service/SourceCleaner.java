package com.pv.nandtotetris.service;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by potharnv on 2016.12.02..
 */
@Component
public class SourceCleaner {

    public List<String> cleanSource(Stream<String> input) {
        List<String> instructions = input.map(s -> cleanLine(s)).filter(s -> !s.equals("")).collect(Collectors.toList());
        return instructions;
    }

    private String cleanLine(String line) {
        String cleanedLine =  line.replaceAll("\\s","");
        if(cleanedLine.indexOf("//") > -1) {
            cleanedLine = cleanedLine.substring(0, cleanedLine.indexOf("//"));
        }
        return cleanedLine;
    }
}
