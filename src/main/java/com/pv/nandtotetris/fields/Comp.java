package com.pv.nandtotetris.fields;

import com.pv.nandtotetris.exceptions.InvalidMnemonicException;

/**
 * Created by potharnv on 2016.11.24..
 */
public enum Comp {

    ZERO("0", "101010", false),
    ONE("1", "111111", false),
    MINUS_ONE("-1", "111010", false),
    D("D", "001100", false),
    A("A", "110000", false),
    M("M", "110000", true),
    NOT_D("!D", "001101", false),
    NOT_A("!A", "110001", false),
    NOT_M("!M", "110001", true),
    MINUS_D("-D", "001111", false),
    MINUS_A("-A", "110011", false),
    MINUS_M("-M", "110011", true),
    D_PLUS_1("D+1", "011111", false),
    A_PLUS_1("A+1", "110111", false),
    M_PLUS_1("M+1", "110111", true),
    D_MINUS_1("D-1", "001110", false),
    A_MINUS_1("A-1", "110010", false),
    M_MINUS_1("M-1", "110010", true),
    D_PLUS_A("D+A", "000010", false),
    D_PLUS_M("D+M", "000010", true),
    D_MINUS_A("D-A", "010011", false),
    D_MINUS_M("D-M", "010011", true),
    A_MINUS_D("A-D", "000111", false),
    M_MINUS_D("M-D", "000111", true),
    D_AND_A("D&A", "000000", false),
    D_AND_M("D&M", "000000", true),
    D_OR_A("D|A", "010101", false),
    D_OR_M("D|M", "010101", true);

    private final String mnemonic;
    private final String code;
    private final boolean abit;

    Comp(String mnemonic, String code, boolean abit) {
        this.mnemonic = mnemonic;
        this.code = code;
        this.abit = abit;
    }

    public static Comp getByMnemonic(String mnemonic) {
        for(Comp comp : values()) {
            if(comp.mnemonic.equals(mnemonic)) return comp;
        }
        throw new InvalidMnemonicException("Invalid computation mnemonic: " + mnemonic);
    }

    public String getMnemonic() {
        return mnemonic;
    }

    public String getCode() {
        return code;
    }

    public boolean isAbit() {
        return abit;
    }

}
