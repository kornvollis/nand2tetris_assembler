package com.pv.nandtotetris.fields;

import com.pv.nandtotetris.exceptions.InvalidMnemonicException;

/**
 * Created by MedveMokus on 2016.11.26..
 */
public enum Dest {

    NULL("null", "000"),
    M("M", "001"),
    D("D", "010"),
    MD("MD", "011"),
    A("A", "100"),
    AM("AM", "101"),
    AD("AD", "110"),
    AMD("AMD", "111");

    private final String mnemonic;
    private final String code;

    Dest(String mnemonic, String code) {
        this.mnemonic = mnemonic;
        this.code = code;
    }

    public static Dest getByMnemonic(String mnemonic) {
        for(Dest dest : values()) {
            if(dest.mnemonic.equals(mnemonic)) return dest;
        }
        throw new InvalidMnemonicException("Invalid destination mnemonic: " + mnemonic);
    }

    public String getCode() {
        return code;
    }

}
