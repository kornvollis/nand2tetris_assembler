package com.pv.nandtotetris.fields;

import com.pv.nandtotetris.exceptions.InvalidMnemonicException;

/**
 * Created by MedveMokus on 2016.11.26..
 */
public enum Jump {

    NULL("null", "000"),
    JGT("JGT", "001"),
    JEQ("JEQ", "010"),
    JGE("JGE", "011"),
    JLT("JLT", "100"),
    JNE("JNE", "101"),
    JLE("JLE", "110"),
    JMP("JMP", "111");

    private final String mnemonic;
    private final String code;

    Jump(String mnemonic, String code) {
        this.mnemonic = mnemonic;
        this.code = code;
    }

    public static Jump getByMnemonic(String mnemonic) {
        for(Jump dest : values()) {
            if(dest.mnemonic.equals(mnemonic)) return dest;
        }
        throw new InvalidMnemonicException("Invalid jump mnemonic: " + mnemonic);
    }

    public String getCode() {
        return code;
    }

}
