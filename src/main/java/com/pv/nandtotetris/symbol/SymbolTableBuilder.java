package com.pv.nandtotetris.symbol;

import java.util.List;

/**
 * Created by potharnv on 2016.12.05..
 */
public class SymbolTableBuilder {

    private SymbolTableBuilder() {
        throw new IllegalAccessError("Utility class");
    }

    public static SymbolTable build(List<String> instructions) {
        SymbolTable symbolTable = new SymbolTable();

        int lineNumber = 0;

        for (String instruction : instructions) {
            if(instruction.charAt(0) == '(') {
                String label = instruction.substring(1, instruction.length()-1);
                symbolTable.addSymbol(label, lineNumber);
            } else {
                lineNumber++;
            }
        }

        return symbolTable;
    }
}
