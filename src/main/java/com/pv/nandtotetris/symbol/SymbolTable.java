package com.pv.nandtotetris.symbol;

import java.util.HashMap;

/**
 * Created by potharnv on 2016.12.01..
 */
public class SymbolTable {

    public static final Integer SP = 0;
    public static final Integer LCL = 1;
    public static final Integer ARG = 2;
    public static final Integer THIS = 3;
    public static final Integer THAT = 4;
    public static final Integer R0 = 0;
    public static final Integer R1 = 1;
    public static final Integer R2 = 2;
    public static final Integer R3 = 3;
    public static final Integer R4 = 4;
    public static final Integer R5 = 5;
    public static final Integer R6 = 6;
    public static final Integer R7 = 7;
    public static final Integer R8 = 8;
    public static final Integer R9 = 9;
    public static final Integer R10 = 10;
    public static final Integer R11 = 11;
    public static final Integer R12 = 12;
    public static final Integer R13 = 13;
    public static final Integer R14 = 14;
    public static final Integer R15 = 15;
    public static final Integer SCREEN = 16384;
    public static final Integer KBD = 24576;

    private HashMap<String, Integer> symbols;

    private int variableIndex = 16;

    public SymbolTable() {
        symbols = new HashMap<>();
        symbols.put("SP", SP);
        symbols.put("LCL", LCL);
        symbols.put("ARG", ARG);
        symbols.put("THIS", THIS);
        symbols.put("THAT", THAT);
        symbols.put("R0", R0);
        symbols.put("R1", R1);
        symbols.put("R2", R2);
        symbols.put("R3", R3);
        symbols.put("R4", R4);
        symbols.put("R5", R5);
        symbols.put("R6", R6);
        symbols.put("R7", R7);
        symbols.put("R8", R8);
        symbols.put("R9", R9);
        symbols.put("R10", R10);
        symbols.put("R11", R11);
        symbols.put("R12", R12);
        symbols.put("R13", R13);
        symbols.put("R14", R14);
        symbols.put("R15", R15);
        symbols.put("SCREEN", SCREEN);
        symbols.put("KBD", KBD);
    }

    public Integer lookUp(String symbol) {
        try {
            return symbols.get(symbol);
        } catch (NullPointerException ex) {
            return null;
        }
    }

    public void addSymbol(String name, Integer value) {
        symbols.put(name, value);
    }

    public void addVariable(String var) {
        if(lookUp(var) == null) {
            symbols.put(var, variableIndex);
            variableIndex++;
        }
    }
}
