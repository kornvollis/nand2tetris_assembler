package com.pv.nandtotetris.instructions;

/**
 * Created by potharnv on 2016.11.24..
 */
public class AInstruction implements HackInstruction {

    private int value;

    public AInstruction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String getCode() {
        return String.format("%16s", Integer.toBinaryString(value) ).replace(' ', '0');
    }
}
