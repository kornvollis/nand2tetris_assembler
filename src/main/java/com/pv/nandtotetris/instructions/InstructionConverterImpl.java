package com.pv.nandtotetris.instructions;

import com.pv.nandtotetris.exceptions.InvalidInstructionException;
import com.pv.nandtotetris.exceptions.InvalidMnemonicException;
import com.pv.nandtotetris.fields.Comp;
import com.pv.nandtotetris.fields.Dest;
import com.pv.nandtotetris.fields.Jump;
import org.springframework.stereotype.Component;

/**
 * Created by potharnv on 2016.11.24..
 */
@Component
public class InstructionConverterImpl implements InstructionConverter {

    @Override
    public HackInstruction parseInstruction(String line) {

        try {
            if (line.charAt(0) == '@') {
                int aInstructionValue = Integer.valueOf(line.substring(1));
                return new AInstruction(aInstructionValue);
            } else if (line.charAt(0) == '(') {
                return new LabelInstruction(line.substring(1, line.length() - 1));
            } else {
                Dest dest = getDestination(line);
                Comp comp = getComp(line);
                Jump jmp = getJump(line);
                return new CInstruction(dest, comp, jmp);
            }
        } catch (InvalidMnemonicException ex) {
            throw new InvalidInstructionException("Invalid instruction: " + line);
        }
    }

    private Comp getComp(String line) {
        int start = 0;
        int end = line.length();

        if (line.indexOf("=") > 0) {
            start = line.indexOf("=") + 1;
        }

        if (line.indexOf(";") > 0) {
            end = line.indexOf(";");
        }

        return Comp.getByMnemonic(line.substring(start, end));
    }

    private Dest getDestination(String line) {
        if (line.indexOf("=") > 0) {
            return Dest.getByMnemonic(line.substring(0, line.indexOf("=")));
        }
        return Dest.NULL;
    }

    private Jump getJump(String line) {
        if (line.indexOf(";") > 0) {
            return Jump.getByMnemonic(line.substring(line.indexOf(";") + 1));
        }
        return Jump.NULL;
    }
}
