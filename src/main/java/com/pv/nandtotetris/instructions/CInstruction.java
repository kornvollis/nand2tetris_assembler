package com.pv.nandtotetris.instructions;

import com.pv.nandtotetris.fields.Comp;
import com.pv.nandtotetris.fields.Dest;
import com.pv.nandtotetris.fields.Jump;

/**
 * Created by MedveMokus on 2016.11.26..
 */
public class CInstruction implements HackInstruction {

    private Comp comp;
    private Dest dest;
    private Jump jmp;

    public CInstruction(Dest dest, Comp comp, Jump jmp) {
        this.dest = dest;
        this.comp = comp;
        this.jmp = jmp;
    }

    @Override
    public String getCode() {
        String abit = (comp.isAbit())? "1" : "0";
        return "111" + abit + comp.getCode() + dest.getCode() + jmp.getCode();
    }

    public Dest getDest() {
        return dest;
    }

    public Comp getComp() {
        return comp;
    }

    public Jump getJmp() {
        return jmp;
    }
}
