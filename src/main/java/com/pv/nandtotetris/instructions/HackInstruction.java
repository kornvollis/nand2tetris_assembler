package com.pv.nandtotetris.instructions;

/**
 * Created by potharnv on 2016.11.24..
 */
public interface HackInstruction {
    String getCode();
}
