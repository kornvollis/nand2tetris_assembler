package com.pv.nandtotetris.instructions;

/**
 * Created by potharnv on 2016.11.24..
 */
public class LabelInstruction implements HackInstruction {

    private String name;

    public LabelInstruction(String name) {
        this.name = name;
    }

    @Override
    public String getCode() {
        return null;//String.format("%16s", Integer.toBinaryString(lineNumber) ).replace(' ', '0');
    }

    public String getName() {
        return name;
    }

}
