package com.pv.nandtotetris.instructions;

import com.pv.nandtotetris.instructions.HackInstruction;

/**
 * Created by potharnv on 2016.11.24..
 */
public interface InstructionConverter {
    HackInstruction parseInstruction(String line);
}