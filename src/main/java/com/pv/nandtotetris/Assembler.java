package com.pv.nandtotetris;

import com.pv.nandtotetris.instructions.*;
import com.pv.nandtotetris.service.SourceCleaner;
import com.pv.nandtotetris.symbol.SymbolTable;
import com.pv.nandtotetris.symbol.SymbolTableBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by potharnv on 2016.12.01..
 */
@Service
public class Assembler {

    @Autowired
    private SourceCleaner sourceCleaner;

    @Autowired
    private InstructionConverter instructionConverter;

    public void compile(Stream<String> asmInput, String fileName) throws IOException {
        List<String> instructions = sourceCleaner.cleanSource(asmInput);

        SymbolTable symbolTable = SymbolTableBuilder.build(instructions);

        List<String> instructionsAfter = new ArrayList<>();
        for (String instruction : instructions) {
            if(isVariable(instruction)) {
                String var = instruction.substring(1, instruction.length());
                symbolTable.addVariable(var);
                instructionsAfter.add("@" + symbolTable.lookUp(var));
            } else if(isLabel(instruction)) {
                instructionsAfter.add(instruction);
            }
        }

        List<String> lines = instructionsAfter.stream().map(instructionConverter::parseInstruction).map(s -> s.getCode()).collect(Collectors.toList());
        writeFile(fileName + ".hack", lines);
     }

    private void writeFile(String fileName, List<String> lines) throws IOException {
        Path file = Paths.get(fileName);
        Files.write(file, lines, Charset.forName("UTF-8"));
    }

    private boolean isVariable(String instruction) {
        return instruction.charAt(0) == '@' && !instruction.substring(1,2).matches("[0-9]");
    }

    private boolean isLabel(String instruction) {
        return instruction.charAt(0) != '(';
    }
}
