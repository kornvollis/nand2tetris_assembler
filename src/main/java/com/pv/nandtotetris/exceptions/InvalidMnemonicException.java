package com.pv.nandtotetris.exceptions;

/**
 * Created by potharnv on 2016.12.02..
 */
public class InvalidMnemonicException extends RuntimeException {
    public InvalidMnemonicException(String message) {
        super(message);
    }
}
