package com.pv.nandtotetris.exceptions;

/**
 * Created by potharnv on 2016.12.02..
 */
public class InvalidInstructionException extends RuntimeException {
    public InvalidInstructionException(String message) {
        super(message);
    }
}
