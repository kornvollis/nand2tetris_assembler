package com.pv.nandtotetris.exceptions;

/**
 * Created by potharnv on 2016.12.02..
 */
public class UnsupportedInstruction extends RuntimeException {
    public UnsupportedInstruction(String s) {
        super(s);
    }
}
