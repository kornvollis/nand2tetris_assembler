package com.pv.nandtotetris;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@SpringBootApplication
public class HackAssemblerApplication implements CommandLineRunner {

	@Autowired
	Assembler assembler;

	@Override
	public void run(String... args) throws IOException {

		if(args.length < 1) {
			throw new RuntimeException("Missing argument, file name");
		}

		URL url = this.getClass().getClassLoader().getResource(args[0]);
		String fileName = url.getFile();
		fileName = System.getProperty( "os.name" ).contains( "indow" ) ? fileName.substring(1) : fileName;

		Stream<String> stream = Files.lines(Paths.get(fileName));

		assembler.compile(stream, getFileNameWithoutExtension(fileName));
	}

	private String getFileNameWithoutExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf("/")+1, fileName.lastIndexOf("."));
    }

	public static void main(String[] args) {
		SpringApplication.run(HackAssemblerApplication.class, args); // NOSONAR
	}
}
