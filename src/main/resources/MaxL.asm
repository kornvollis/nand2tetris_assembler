// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/06/max/MaxL.asm

// Symbol-less version of the Max.asm program.

@0      // 0000000000000000
D=M     // 1111110000010000
@1      // 0000000000000001
D=D-M   // 1111010011010000
@10     // 0000000000001010
D;JGT   // 1110001100000001
@1      // 0000000000000001
D=M     // 1111110000010000
@12     // 0000000000001100
0;JMP   // 1110101010000111
@0      // 0000000000000000
D=M     // 1111110000010000
@2      // 0000000000000010
M=D     // 1110001100001000
@14     // 0000000000001110
0;JMP   // 1110101010000111
